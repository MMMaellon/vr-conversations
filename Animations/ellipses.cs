
using UdonSharp;
using UnityEngine;
using VRC.SDKBase;
using VRC.Udon;
using UnityEngine.UI;

public class ellipses : UdonSharpBehaviour
{
    private Text txt;
    private float frameMod = 0;
    private int dots = 0;
    void Start()
    {
        txt = GetComponent<Text>();
        if(txt != null){
            txt.text = ".";
        }
    }

    private void Update(){
        float new_frameMod = (frameMod + Time.deltaTime) % 1f;
        if(new_frameMod < frameMod && txt != null){
            dots = (dots + 1) % 3;
            if(dots == 0){
                txt.text = ".";
            } else if(dots == 1){
                txt.text = "..";
            } else if(dots == 2){
                txt.text = "...";
            }
        }
        frameMod = new_frameMod;
    }
}
