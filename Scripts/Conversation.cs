
using UdonSharp;
using UnityEngine;
using VRC.SDKBase;
using VRC.Udon;
using UnityEngine.UI;
using VRC.Udon.Common.Interfaces;

public class Conversation : UdonSharpBehaviour
{

    //Saying certain lines will earn you certain points. You decide what they mean.
    [UdonSynced(UdonSyncMode.None)] int conversationPoints1 = 0;
    [UdonSynced(UdonSyncMode.None)] int conversationPoints2 = 0;
    [UdonSynced(UdonSyncMode.None)] int conversationPoints3 = 0;
    public int startConversationPoints1 = 0;
    public int startConversationPoints2 = 0;
    public int startConversationPoints3 = 0;


    [Header("Make sure every line that's part of this conversation is part of this array")]
    private Conversation_Line[] lines;

    [Header("Speakers will be autofilled from children if empty")]
    public Speaker[] speakers;
    public Speaker defaultSpeaker;


    [Header("Don't Touch These Ones")]
    [UdonSynced(UdonSyncMode.None)] int currentLine = -1; //keep track of which line is being said for late joiners
    [UdonSynced(UdonSyncMode.None)] int lastLine = -1; //last line stays visible so you can read it and respond to it
    [UdonSynced(UdonSyncMode.None)] int interruptionLine = -1; //interruption line. When a conversation gets interrupted this gets set at the point where it happens so that we can come back to it later.
    [UdonSynced(UdonSyncMode.None)] int speaker = -1;

    public int language = 0;

    public Dialog_Box currentDialogBox;
    public Dialog_Box lastDialogBox;
    public Dialog_Options choices;

    private int currentLineLocal = -1001; //keep track of which line is being said for late joiners
    private int lastLineLocal = -1001; //last line stays visible so you can read it and respond to it
    private int interruptionLineLocal = -1; //last line stays visible so you can read it and respond to it
    private int speakerLocal = -1;

    private string currentString = "";
    private string lastString = "";

    private float waitTimer = 0;
    private float talkTimer = 0;

    public Text debug;

    private string debug_print = "";

    private float frameMod = 0;

    void Start()
    {
        if( speakers == null || speakers.Length == 0){
            speakers = GetComponentsInChildren<Speaker>(true);
        }

        int totalLineCount = 0;
        for (int i = 0; i < speakers.Length; i++){
            if(speakers[i] != null){
                speakers[i].SetId(i, this);
                if (speakers[i].lines.Length == 0)
                {
                    speakers[i].AutoFill();
                }
                totalLineCount += speakers[i].lines.Length;
            }
        }
        if(defaultSpeaker != null){
            speaker = defaultSpeaker.GetId();
        }

        lines = new Conversation_Line[totalLineCount];

        int line_index = 0;

        for (int i = 0; i < speakers.Length; i++){
            if(speakers[i] != null){
                for (int j = 0; j < speakers[i].lines.Length; j++)
                {
                    if(speakers[i].lines[j] != null){
                        lines[line_index] = speakers[i].lines[j];
                        lines[line_index].SetId(line_index, this);
                        lines[line_index].gameObject.SetActive(false);
                        line_index += 1;
                    }
                }
            }
        }


        conversationPoints1 = startConversationPoints1;
        conversationPoints2 = startConversationPoints2;
        conversationPoints3 = startConversationPoints3;

        currentDialogBox.Register(this);
        lastDialogBox.Register(this);
        choices.Register(this);
        choices.HideChoices();
    }

    public void Reset(){
        SendCustomNetworkEvent(NetworkEventTarget.Owner, "ResetOwner");
    }

    public void ResetOwner(){
        currentLine = 0;
        lastLine = 0;
        interruptionLine = -3;
        conversationPoints1 = startConversationPoints1;
        conversationPoints2 = startConversationPoints2;
        conversationPoints3 = startConversationPoints3;
    }

    public Conversation_Line GetLine(int index){
        if(index >= 0 && index < lines.Length && lines != null){
            return lines[index];
        }
        return null;
    }

    public Conversation_Line GetCurrentLine(){
        return GetLine(currentLine);
    }


    private void FixedUpdate(){
        Conversation_Line current = GetCurrentLine();
        Conversation_Line last = GetLine(lastLine);

        if (currentLine != currentLineLocal)
        {
            currentLineLocal = currentLine;
            if (current != null)
            {
                current.gameObject.SetActive(true);
                current.startAnimation();
                if (current.absoluteContributions)
                {
                    conversationPoints1 = current.conversationPoints1Contribution;
                    conversationPoints2 = current.conversationPoints2Contribution;
                    conversationPoints3 = current.conversationPoints3Contribution;
                }
                else
                {
                    conversationPoints1 += current.conversationPoints1Contribution;
                    conversationPoints2 += current.conversationPoints2Contribution;
                    conversationPoints3 += current.conversationPoints3Contribution;
                }

                waitTimer = current.preTime;
                talkTimer = current.talkTypeDelay * current.GetText(language).Length + current.postTime;

                if (waitTimer <= 0)
                {
                    if (current.GetChoiceSpeaker() == speaker && current.postTime <= 0 && !current.HasOneChoice())
                    {
                        choices.ShowChoices();
                    }
                    else
                    {
                        choices.HideChoices();
                    }

                    if (last == null || current.speaker == null || last.speaker == null || last.speaker.name != current.speaker.name)
                    {
                        if(current.GetText(language).Length > 0){
                            currentDialogBox.TransitionIn(current, language);
                        } else {
                            currentDialogBox.Hide();
                        }
                        if (last != null && last.GetText(language).Length > 0)
                        {
                            lastDialogBox.TransitionOut();
                        }
                        else
                        {
                            lastDialogBox.Hide();
                        }
                    }
                    else
                    {
                        if(currentDialogBox.isHidden){
                            currentDialogBox.TransitionIn(current, language);
                        } else {
                            currentDialogBox.SetFromLine(current, language);
                        }
                        lastDialogBox.Hide();
                    }
                }
                else
                {
                    currentDialogBox.Hide();
                    if (last != null && last.GetText(language).Length > 0)
                    {
                        lastDialogBox.TransitionOut();
                    }
                    else
                    {
                        lastDialogBox.Hide();
                    }
                }
            } else {
                Debug.Log("Ending Convo");
                currentDialogBox.Hide();
                if (last != null && last.GetText(language).Length > 0)
                {
                    Debug.Log("Transitioning Out");
                    lastDialogBox.TransitionOut();
                }
                else
                {
                    Debug.Log("Hiding. Null: " + (last == null) + " id: " + lastLine);
                    lastDialogBox.Hide();
                }
            }
        }

        if (lastLine != lastLineLocal)
        {
            lastLineLocal = lastLine;
            if (last != null)
            {
                last.gameObject.SetActive(false);
            }
        }

        if (current != null)
        {
            Speaker current_speaker = current.speaker;
            if(current_speaker != null){
                currentDialogBox.transform.position = current_speaker.transform.position;
            }
        }

        if (last != null)
        {
            Speaker last_speaker = last.speaker;
            if (last_speaker != null)
            {
                lastDialogBox.transform.position = last_speaker.transform.position;
            }
        }

        if (interruptionLine != interruptionLineLocal)
        {
            interruptionLineLocal = interruptionLine;
            lastDialogBox.Hide();
        }

        if (speaker != speakerLocal)
        {
            speakerLocal = speaker;
            if (talkTimer <= 0)
            {
                if (current != null && current.GetChoiceSpeaker() == speaker && !current.HasOneChoice())
                {
                    choices.ShowChoices();
                }
                else
                {
                    PlayDefault();
                }
            }
        }

        if (waitTimer > 0)
        {
            waitTimer -= Time.deltaTime;
            if (waitTimer <= 0 && current != null)
            {
                currentDialogBox.TransitionIn(current, language);
            }
        }
        else if (talkTimer > 0)
        {
            talkTimer -= Time.deltaTime;
            if (talkTimer <= 0 && current != null)
            {
                if (current.GetChoiceSpeaker() == speaker && !current.HasOneChoice())
                {
                    choices.ShowChoices();
                }
                else
                {
                    PlayDefault();
                }
            }
        }

        if (Input.GetKeyDown(KeyCode.Return))
        {
            Continue();
        }

        if(debug != null && debug.gameObject.activeSelf){
            string current_string = "null";
            string speaker_string = "null";
            if (current != null)
            {
                current_string = current.GetText(language);
                Speaker current_speaker = current.speaker;
                if (current_speaker != null)
                {
                    speaker_string = current_speaker.name;
                }
            }
            string debug_string = "currentLine: " + currentLine +  " last: " + lastLine + "\nspeaker: " + speaker_string + " text: " + current_string;
            string timer_string = "waitTime: " + waitTimer + " talkTime:" + talkTimer;
            string score_string = "score1: " + conversationPoints1 + " score2: " + conversationPoints2 + " score3: " + conversationPoints3;
            string player_speaker_string = "speaker: " + speaker;
            debug.text = debug_string + "\n" + debug_print + "\n" + timer_string + "\n" + score_string + "\n" + player_speaker_string;
        }
    }

    public void Continue(){
        SendCustomNetworkEvent(NetworkEventTarget.Owner, "ContinueOwner");
    }

    public void ContinueOwner(){
        Conversation_Line current = GetCurrentLine();
        if(waitTimer <= 0 && current != null && current.skippable){
            if(currentDialogBox.IsDone()){
                talkTimer = 0.01f;
            }
        }
    }

    public void PlayDefault(){
        choices.HideChoices();
        Conversation_Line current = GetCurrentLine();
        if (current != null && Networking.LocalPlayer.IsOwner(gameObject))
        {
            PlayLineOwner(current.GetDefaultChoice());
        }
    }

    public void PlayLineOwner(int id){
        Debug.Log("we out here playin this line " + id);
        if(id == -2){
            ResumeInterrruptionOwner();
        } else {
            lastLine = currentLine;
            currentLine = id;
        }
    }
    public void InterruptOwner(int id){
        interruptionLine = currentLine;
        lastLine = interruptionLine;
        currentLine = id;
    }
    public void ResumeInterrruptionOwner(){
        lastLine = currentLine;
        currentLine = interruptionLine;
    }

    public int GetCurrentLineIndex(){
        return currentLine;
    }

    public int GetLastLineIndex(){
        return lastLine;
    }

    public int GetInterruptionLineIndex(){
        return interruptionLine;
    }

    public int GetConversationPoints1(){
        return conversationPoints1;
    }

    public int GetConversationPoints2(){
        return conversationPoints2;
    }

    public int GetConversationPoints3(){
        return conversationPoints3;
    }

    public void SetSpeakerOwner(int i){
        speaker = i;
    }

    public Speaker GetSpeaker(int id){
        if(id >=0 && id < speakers.Length){
            return speakers[id];
        } else {
            return null;
        }
    }
}
