using UnityEngine;
using System.Collections.Generic;
using UdonSharp;
using VRC.SDK3.Components;
using VRC.SDKBase;
using VRC.Udon;
#if !COMPILER_UDONSHARP && UNITY_EDITOR // These using statements must be wrapped in this check to prevent issues on builds
using UnityEditor;
using UdonSharpEditor;

public class Conversations : EditorWindow
{
    GameObject script_parent;
    bool skippable = true;
    bool createNewSpeakers = false;
    bool splitSpeakers = false;

    float autoAdvanceTime = 2.0f;
    float pauseTime = 2.0f;

    string file_name = "untitled conversation";

    GameObject speaker_parent;

    SortedDictionary<string, Speaker> speakers = new SortedDictionary<string, Speaker>();

    bool foldout = true;

    SortedDictionary<string, List<Conversation_Line_Proxy>> parsed_script = null;

    Vector2 scrollPos;

    // Add menu item named "My Window" to the Window menu
    [MenuItem("Window/Conversation Generator")]
    public static void ShowWindow()
    {
        //Show existing window instance. If one doesn't exist, make one.
        EditorWindow.GetWindow(typeof(Conversations));
    }

    void OnGUI()
    {
        scrollPos = EditorGUILayout.BeginScrollView(scrollPos);
        GUILayout.Label("Generate Conversation", EditorStyles.boldLabel);
        script_parent = (GameObject)EditorGUILayout.ObjectField("Script Parent", script_parent, typeof(GameObject), true);
        speaker_parent = (GameObject)EditorGUILayout.ObjectField("Speaker Parent", speaker_parent, typeof(GameObject), true);
        skippable = EditorGUILayout.Toggle("Skippable", skippable);
        splitSpeakers = EditorGUILayout.Toggle("Split By Speakers", splitSpeakers);
        autoAdvanceTime = EditorGUILayout.Slider("Auto Advance Time", autoAdvanceTime, 0.0f, 10f);
        pauseTime = EditorGUILayout.Slider("Pause Time", pauseTime, 0.1f, 10f);

        if(GUILayout.Button("Parse Fountain Scripts")){
            SelectFile();
        }
        foldout = EditorGUILayout.Foldout(foldout, "Speakers To Link To");
        if(foldout && parsed_script != null){
            foreach(string key in parsed_script.Keys){
                if (speakers != null)
                {
                    if (!speakers.ContainsKey(key))
                    {
                        speakers.Add(key, null);
                    }
                    speakers[key] = EditorGUILayout.ObjectField(key, speakers[key], typeof(Speaker), true) as Speaker;
                }
            }
        }
        if(parsed_script != null){
            if (GUILayout.Button("Generate Conversation")){
                GenerateConversation();
            }
        }
        EditorGUILayout.EndScrollView();
    }

    void GenerateConversation(){
        GameObject file_parent = new GameObject(file_name);
        if(script_parent != null){
            file_parent.transform.SetParent(script_parent.transform);
        }
        if(speaker_parent == null){
            speaker_parent = new GameObject("SPEAKERS");
            speaker_parent.transform.SetParent(file_parent.transform);
        }

        SortedDictionary<int, Conversation_Line> ordered_script = new SortedDictionary<int, Conversation_Line>();
        foreach(string speaker in parsed_script.Keys){
            Transform current_parent = file_parent.transform;
            Speaker current_speaker = null;
            if (speakers.ContainsKey(speaker) && speakers[speaker] != null)
            {
                current_speaker = speakers[speaker];
            } else {
                current_speaker = new GameObject(speaker).AddUdonSharpComponent<Speaker>();
                current_speaker.transform.SetParent(speaker_parent.transform);
                current_speaker.UpdateProxy();
                current_speaker.displayName = speaker;
                current_speaker.ApplyProxyModifications();
            }

            if(splitSpeakers){
                current_parent = new GameObject(speaker).transform;
                current_parent.SetParent(file_parent.transform);
            }
            List<Conversation_Line> current_lines = new List<Conversation_Line>(current_speaker.lines);
            current_lines.RemoveAll(item => item == null);
            foreach(Conversation_Line_Proxy proxy in parsed_script[speaker]){
                if(proxy != null){
                    Conversation_Line new_line = new GameObject(proxy.count.ToString("000") + " " + proxy.text.Substring(0, Mathf.Min(20, proxy.text.Length))).AddUdonSharpComponent<Conversation_Line>();
                    new_line.transform.SetParent(current_parent);
                    new_line.UpdateProxy();
                    new_line.lineText = proxy.text;
                    if (proxy.wait)
                    {
                        new_line.preTime = pauseTime;
                    }
                    new_line.postTime = autoAdvanceTime;
                    new_line.speaker = current_speaker;
                    new_line.skippable = skippable;
                    new_line.SetChoice1(new_line);
                    new_line.SetChoice2(new_line);
                    new_line.SetChoice3(new_line);
                    new_line.SetChoice4(new_line);
                    new_line.SetAltChoice1(new_line);
                    new_line.SetAltChoice2(new_line);
                    new_line.SetAltChoice3(new_line);
                    new_line.SetAltChoice4(new_line);
                    new_line.ApplyProxyModifications();
                    current_lines.Add(new_line);
                    ordered_script.Add(proxy.count, new_line);
                }
            }
            current_speaker.UpdateProxy();
            current_speaker.lines = current_lines.ToArray();
            current_speaker.ApplyProxyModifications();
        }
        // for (int i = 1; i < ordered_script.Values.Count; i++){ //Can't use this one
        for (int i = ordered_script.Values.Count - 1; i > 0; i--) //You have to update them in reverse order for some reason???
        {
            ordered_script[i].UpdateProxy();
            ordered_script[i].SetChoice1(ordered_script[i + 1]);
            ordered_script[i].ApplyProxyModifications();
        }
    }

    void SelectFile(){
        string file = EditorUtility.OpenFilePanelWithFilters("Select Fountain Files", "", new string[] {"fountain scripts","fountain,txt"});
        if(file != ""){
            int start = file.LastIndexOf("/") + 1;
            int stop = file.LastIndexOf(".");
            file_name = file.Substring(start, Mathf.Max(0, stop - start));
            parsed_script = ParseScript(file);
            speakers.Clear();
        }
    }

    SortedDictionary<string, List<Conversation_Line_Proxy>> ParseScript(string file){
        Debug.Log("parsing " + file);
        SortedDictionary<string, List<Conversation_Line_Proxy>> dict = new SortedDictionary<string, List<Conversation_Line_Proxy>>();
        IEnumerable<string> lines = System.IO.File.ReadLines(file);
        Conversation_Line_Proxy current_line = null;
        int line_count = 0;
        foreach(string line in lines){
            string trimmed = line.Trim();
            if(current_line == null){
                if(trimmed.Length > 0 && trimmed.ToUpper() == trimmed && !(trimmed.StartsWith("INT") || trimmed.StartsWith("EXT") || trimmed.StartsWith("EST") || trimmed.StartsWith("I/E") || trimmed.StartsWith("_") || trimmed.StartsWith("*") || trimmed.EndsWith("!") || trimmed.EndsWith(":") || trimmed.StartsWith(">") || trimmed.StartsWith(">"))){
                    current_line = new Conversation_Line_Proxy();
                    current_line.speaker = trimmed;
                }
            } else {
                int combined_length = (trimmed.Length + current_line.text.Length);
                if(trimmed.Length == 0 || trimmed.StartsWith("(") || trimmed.StartsWith("#") || trimmed.StartsWith("[") || trimmed.StartsWith(">") || trimmed.StartsWith("!") || trimmed.StartsWith("_") || trimmed.StartsWith("*") || (combined_length > 140)){ //length of a tweet way back in the day
                    if (current_line.text.Length > 0)
                    {
                        line_count += 1;
                        current_line.count = line_count;
                        if (dict.ContainsKey(current_line.speaker))
                        {
                            dict[current_line.speaker].Add(current_line);
                        }
                        else
                        {
                            dict.Add(current_line.speaker, new List<Conversation_Line_Proxy>() { current_line });
                        }
                    }

                    if(trimmed.Length == 0 || trimmed.StartsWith("#")){
                        current_line = null;
                    } else
                    {
                        Conversation_Line_Proxy old_line = current_line;
                        current_line = new Conversation_Line_Proxy();
                        current_line.speaker = old_line.speaker;
                        current_line.wait = (trimmed == "(beat)" || trimmed == "(BEAT)" || trimmed == "(Beat)" || trimmed == "(pause)" || trimmed == "(PAUSE)" || trimmed == "(Pause)");
                        if(combined_length > 140){
                            current_line.text = trimmed;
                        }
                    }
                } else {
                    if(current_line.text.Length > 0){
                        current_line.text += " ";
                    }
                    current_line.text += trimmed;
                }
            }
        }
        return dict;
    }
}

public class Conversation_Line_Proxy{
    public int count = 0;
    public string speaker = "";
    public string text = "";
    public bool wait = false;

    public Conversation_Line final_form = null;
}

#endif