
using UdonSharp;
using UnityEngine;
using VRC.SDKBase;
using VRC.Udon;
using VRC.Udon.Common.Interfaces;

public class Conversation_Line : UdonSharpBehaviour
{
    public float preTime = 0; //how much time to wait before talking
    public float postTime = 2f; //let's just make this local. No one would really know the diff
    public float chooseTime = 0; //give players time to make a choice. 0 or less means wait indefinitely

    public bool skippable = true;

    [TextArea(3,5)] public string lineText = "";
    [TextArea(3,5)] public string lineText_language1 = "";
    [TextArea(3,5)] public string lineText_language2 = "";
    [TextArea(3,5)] public string lineText_language3 = "";

    [TextArea(3, 5)] public string displayText = "";
    [TextArea(3, 5)] public string displayText_language1 = "";
    [TextArea(3, 5)] public string displayText_language2 = "";
    [TextArea(3, 5)] public string displayText_language3 = "";

    public int fontSize = 10;

    public int minConversationPoints1 = 0;
    public int minConversationPoints2 = 0;
    public int minConversationPoints3 = 0;
    public int maxConversationPoints1 = 100;
    public int maxConversationPoints2 = 100;
    public int maxConversationPoints3 = 100;

    public bool absoluteContributions = false;
    public int conversationPoints1Contribution = 0;
    public int conversationPoints2Contribution = 0;
    public int conversationPoints3Contribution = 0;
    public int defaultChoice = 1;
    [SerializeField] private Conversation_Line choice1 = null;
    [SerializeField] private Conversation_Line choice2 = null;
    [SerializeField] private Conversation_Line choice3 = null;
    [SerializeField] private Conversation_Line choice4 = null;
    [SerializeField] private Conversation_Line altChoice1 = null;
    [SerializeField] private Conversation_Line altChoice2 = null;
    [SerializeField] private Conversation_Line altChoice3 = null;
    [SerializeField] private Conversation_Line altChoice4 = null;
    public Animator animator;
    public string animationTrigger;

    public Speaker speaker = null;
    public AudioSource specialTalkSound = null;
    public float talkTypeDelay = 0.03f;
    public float talkSoundDelay = 0.1f;

    private int id = -69;
    private Conversation conversation_controller = null;
    void Start()
    {
        Conversation_Line choice1 = this;
        Conversation_Line choice2 = this;
        Conversation_Line choice3 = this;
        Conversation_Line choice4 = this;
        Conversation_Line altChoice1 = this;
        Conversation_Line altChoice2 = this;
        Conversation_Line altChoice3 = this;
        Conversation_Line altChoice4 = this;
    }

    public void RegisterSpeaker(Speaker new_speaker){
        if(new_speaker != null && speaker == null){
            speaker = new_speaker;
        }
    }

    public Conversation_Line getChoice1()
    {
        if (conversation_controller == null)
        {
            return null;
        }
        if (choice1 && choice1.isValidLineAuto() && choice1.GetId() != id)
        {
            return choice1;
        }
        if (altChoice1 && altChoice1.isValidLineAuto() && altChoice1.GetId() != id)
        {
            return altChoice1;
        }

        return null;
    }

    public Conversation_Line getChoice2()
    {
        if (conversation_controller == null)
        {
            return null;
        }
        if (choice2 && choice2.isValidLineAuto() && choice2.GetId() != id)
        {
            return choice2;
        }
        if (altChoice2 && altChoice2.isValidLineAuto() && altChoice2.GetId() != id)
        {
            return altChoice2;
        }

        return null;
    }

    public Conversation_Line getChoice3()
    {
        if (conversation_controller == null)
        {
            return null;
        }
        if (choice3 && choice3.isValidLineAuto() && choice3.GetId() != id)
        {
            return choice3;
        }
        if (altChoice3 && altChoice3.isValidLineAuto() && altChoice3.GetId() != id)
        {
            return altChoice3;
        }

        return null;
    }

    public Conversation_Line getChoice4()
    {
        if (conversation_controller == null)
        {
            return null;
        }
        if (choice4 && choice4.isValidLineAuto() && choice4.GetId() != id)
        {
            return choice4;
        }
        if (altChoice4 && altChoice4.isValidLineAuto() && altChoice4.GetId() != id)
        {
            return altChoice4;
        }

        return null;
    }

    private bool isValidLine(int currentPoints1, int currentPoints2, int currentPoints3){
        bool points1Valid = ((currentPoints1 >= minConversationPoints1 && currentPoints1 < maxConversationPoints1) || maxConversationPoints1 == minConversationPoints1);
        bool points2Valid = ((currentPoints2 >= minConversationPoints2 && currentPoints2 < maxConversationPoints2) || maxConversationPoints2 == minConversationPoints2);
        bool points3Valid = ((currentPoints3 >= minConversationPoints3 && currentPoints3 < maxConversationPoints3) || maxConversationPoints3 == minConversationPoints3);
        return points1Valid && points2Valid && points3Valid;
    }

    public bool isValidLineAuto(){
        return conversation_controller != null && isValidLine(conversation_controller.GetConversationPoints1(), conversation_controller.GetConversationPoints2(), conversation_controller.GetConversationPoints3());
    }

    public void SetId(int new_id, Conversation conversation){
        id = new_id;
        conversation_controller = conversation;
    }

    public int GetId(){
        return id;
    }

    public string GetText(int language)
    {
        if (language == 1 && lineText_language1.Length != 0)
        {
            return lineText_language1;
        }
        else if (language == 2 && lineText_language2.Length != 0)
        {
            return lineText_language2;
        }
        else if (language == 3 && lineText_language3.Length != 0)
        {
            return lineText_language3;
        }
        else
        {
            return lineText;
        }
    }

    public string GetDisplayText(int language)
    {
        if (language == 1) {
            if(displayText_language1.Length != 0)
            {
                return displayText_language1;
            }
        }
        else if (language == 2) {
            if(displayText_language2.Length != 0)
            {
                return displayText_language2;
            }
        }
        else if (language == 3) {
            if(displayText_language3.Length != 0)
            {
                return displayText_language3;
            }
        }
        else if (displayText.Length != 0)
        {
            return displayText;
        }
        return GetText(language);
    }

    public int GetDefaultChoice(){
        int choice = -1;
        if(GetChoice(defaultChoice) != null){
            choice = GetChoice(defaultChoice).GetId();
        } else{
            int choiceCount = GetChoiceCount();
            if(choiceCount > 1){
                choice = -3;
                while (choice == -3)
                {
                    float random = Random.Range(0, 4);
                    if (random < 1)
                    {
                        if (getChoice1() != null)
                        {
                            choice = getChoice1().GetId();
                        }
                    }
                    else if (random < 2)
                    {
                        if (getChoice2() != null)
                        {
                            choice = getChoice2().GetId();
                        }
                    }
                    else if (random < 3)
                    {
                        if (getChoice3() != null)
                        {
                            choice = getChoice3().GetId();
                        }
                    }
                    else
                    {
                        if (getChoice4() != null)
                        {
                            choice = getChoice4().GetId();
                        }
                    }
                }
            } else {
                choice = GetFirstChoice();
            }
        }
        return choice;
    }

    public int GetChoiceCount(){
        int choiceCount = 0;
        if (getChoice1() != null)
        {
            choiceCount += 1;
        }
        if (getChoice2() != null)
        {
            choiceCount += 1;
        }
        if (getChoice3() != null)
        {
            choiceCount += 1;
        }
        if (getChoice4() != null)
        {
            choiceCount += 1;
        }
        return choiceCount;
    }

    public int GetFirstChoice(){
        int choice = -1;
        if (getChoice1() != null)
        {
            return getChoice1().GetId();
        }
        if (getChoice2() != null)
        {
            return getChoice2().GetId();
        }
        if (getChoice3() != null)
        {
            return getChoice3().GetId();
        }
        if (getChoice4() != null)
        {
            return getChoice4().GetId();
        }
        return choice;
    }

    public bool HasOneChoice(){
        return GetChoiceCount() == 1;
    }

    public void Play(){
        SendCustomNetworkEvent(NetworkEventTarget.Owner, "PlayOwner");
    }

    public void PlayInterrupt(){
        SendCustomNetworkEvent(NetworkEventTarget.Owner, "PlayInterruptOwner");
    }

    public void PlayOwner(){
        Debug.Log("Calling Play Owner with " + id);
        if(conversation_controller != null){
            conversation_controller.PlayLineOwner(id);
        }
    }

    public void PlayInterruptOwner(){
        if (conversation_controller != null)
        {
            conversation_controller.InterruptOwner(id);
        }
    }

    public Transform GetChoiceSpeakerTransform()
    {
        Conversation_Line choice = conversation_controller.GetLine(GetDefaultChoice());
        if (choice != null)
        {
            return choice.transform;
        }
        return transform;
    }

    public int GetChoiceSpeaker()
    {
        Conversation_Line choice = conversation_controller.GetLine(GetDefaultChoice());
        if (choice != null)
        {
            if(choice.speaker != null){
                return choice.speaker.GetId();
            }
        }
        return -1001;
    }


    public Conversation_Line GetChoice(int index)
    {
        if (conversation_controller != null)
        {
            if (index == 1)
            {
                return getChoice1();
            }
            else if (index == 2)
            {
                return getChoice2();
            }
            else if (index == 3)
            {
                return getChoice3();
            }
            else if (index == 4)
            {
                return getChoice4();
            }
        }
        return null;
    }

    public void startAnimation(){
        if(animator != null){
            animator.SetTrigger(animationTrigger);
        }
    }

    public void SetChoice1(Conversation_Line choice)
    {
        choice1 = choice;
    }

    public void SetChoice2(Conversation_Line choice)
    {
        choice2 = choice;
    }

    public void SetChoice3(Conversation_Line choice)
    {
        choice3 = choice;
    }

    public void SetChoice4(Conversation_Line choice)
    {
        choice4 = choice;
    }

    public void SetAltChoice1(Conversation_Line choice)
    {
        altChoice1 = choice;
    }

    public void SetAltChoice2(Conversation_Line choice)
    {
        altChoice2 = choice;
    }

    public void SetAltChoice3(Conversation_Line choice)
    {
        altChoice3 = choice;
    }

    public void SetAltChoice4(Conversation_Line choice)
    {
        altChoice4 = choice;
    }
}
