
using UdonSharp;
using UnityEngine;
using VRC.SDKBase;
using VRC.Udon;
using UnityEngine.UI;
using VRC.Udon.Common.Interfaces;

public class Dialog_Box : UdonSharpBehaviour
{
    private Conversation controller = null;
    private string dialogText = "";

    private Animator animator = null;
    private AudioSource talkSound = null;
    private AudioSource defaultTalkSound = null;

    public Text speakerObject = null;
    public Text textObject = null;

    public GameObject continueText = null;

    private string speakerName = "";
    private float frameMod = 0;
    private float soundMod = 0;

    public float talkTypeDelay = 0.02f;
    public float talkSoundDelay = 0.05f;

    public bool isHidden = false;
    public bool skippable = true;
    void Start()
    {
        animator = GetComponent<Animator>();

        if(continueText != null){
            continueText.SetActive(false);
        }
        if(animator != null){
            animator.SetTrigger("hide");
        }
    }

    public void Register(Conversation new_controller){
        controller = new_controller;
    }

    public void SetFromLine(Conversation_Line line, int language){
        if(line != null){
            dialogText = line.GetText(language);
            skippable = line.skippable;
            if(textObject.fontSize != 10){
                textObject.fontSize = line.fontSize;
                textObject.resizeTextForBestFit = false;
            } else {
                textObject.fontSize = 10;
                textObject.resizeTextForBestFit = true;
            }
            textObject.fontSize = line.fontSize;
            if(line.talkTypeDelay > 0){
                talkTypeDelay = line.talkTypeDelay;
            } else {
                talkTypeDelay = 0.02f;
            }
            if(line.talkSoundDelay > 0){
                talkSoundDelay = line.talkSoundDelay;
            } else {
                talkSoundDelay = 0.05f;
            }
            if(line.speaker != null){
                speakerName = line.speaker.displayName;
                talkSound = line.speaker.talkSound;
            } else {
                speakerName = "Unknown";
            }
            if (line.specialTalkSound != null)
            {
                talkSound = line.specialTalkSound;
            }
            if (continueText != null && skippable)
            {
                continueText.SetActive(true);
            }
            if (talkSound != null)
            {
                talkSound.Play();
            }
        }
    }

    public void TransitionIn(Conversation_Line line, int language){
        isHidden = false;
        SetFromLine(line, language);
        if (animator != null)
        {
            animator.SetTrigger("transition_in");
        }
        if (continueText != null)
        {
            continueText.SetActive(false);
        }
    }

    public void Show(Conversation_Line line, int language){
        isHidden = false;
        SetFromLine(line, language);
        if (textObject != null)
        {
            textObject.text = dialogText;
        }
        if (speakerObject != null)
        {
            speakerObject.text = speakerName;
        }
        if (animator != null)
        {
            animator.SetTrigger("show");
        }
        if (continueText != null)
        {
            continueText.SetActive(false);
        }
    }

    public void TransitionOut()
    {
        isHidden = true;
        if (textObject != null)
        {
            textObject.text = dialogText;
        }
        if (animator != null)
        {
            animator.SetTrigger("transition_out");
        }
    }

    public void Hide(){
        isHidden = true;
        if (textObject != null)
        {
            textObject.text = dialogText;
        }
        if (animator != null)
        {
            animator.SetTrigger("hide");
        }
    }

    private void FixedUpdate(){
        if(speakerObject != null){
            speakerObject.text = speakerName;
        }

        frameMod += Time.deltaTime;
        for (int i = 0; i < Mathf.FloorToInt(frameMod / talkTypeDelay); i++)
        {
                if (textObject != null && textObject.text != dialogText)
                {
                    if (continueText != null)
                    {
                        continueText.SetActive(false);
                    }
                    if (dialogText.StartsWith(textObject.text))
                    {
                        textObject.text = textObject.text + dialogText.Substring(textObject.text.Length, 1);
                    }
                    else
                    {
                        textObject.text = "";
                        if (talkSound != null)
                        {
                            talkSound.Play();
                        }
                        break;
                    }
                }
                else if (continueText != null && skippable)
                {
                    continueText.SetActive(true);
                    break;
                }
        }
        frameMod = frameMod % talkTypeDelay;

        float new_soundMod = (soundMod + Time.deltaTime) % talkSoundDelay;
        if(textObject.text != dialogText && talkSound != null && new_soundMod < soundMod){
            talkSound.Play();
        }
        soundMod = new_soundMod;
    }

    public bool IsDone(){
        return textObject.text == dialogText;
    }

    override public void Interact(){
        controller.Continue();
    }

    public bool HasActiveTrigger(){
        bool activeTrigger = false;
        if(animator != null){
            return animator.IsInTransition(0);
        }
        return activeTrigger;
    }

}
