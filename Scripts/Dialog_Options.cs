
using UdonSharp;
using UnityEngine;
using VRC.SDKBase;
using VRC.Udon;
using UnityEngine.UI;
using VRC.Udon.Common.Interfaces;

public class Dialog_Options : UdonSharpBehaviour
{
    private Conversation controller;
    public Text choice1Text;
    public Text choice2Text;
    public Text choice3Text;
    public Text choice4Text;

    public AudioSource showSound;

    private float timerMax = 0;
    private bool allowChoice = false;
    private Animator animator;

    public Slider countdownTimer;

    private Conversation_Line choice1 = null;
    private Conversation_Line choice2 = null;
    private Conversation_Line choice3 = null;
    private Conversation_Line choice4 = null;

    private Conversation_Line defaultChoice = null;

    private float chooseTimer = 0f;
    void Start()
    {
        animator = GetComponent<Animator>();
    }
    public void Register(Conversation new_controller)
    {
        controller = new_controller;
    }

    private void Update(){
        if(countdownTimer != null){
            countdownTimer.gameObject.SetActive(countdownTimer.value > 0);
            if(countdownTimer.value > 0){
                countdownTimer.value -= Time.deltaTime;
                Debug.Log("choose timer: " + countdownTimer.value);
                if (countdownTimer.value <= 0)
                {
                    if (allowChoice && animator != null)
                    {
                        allowChoice = false;
                        animator.SetBool("visible", false);
                        if (Networking.LocalPlayer.IsOwner(gameObject))
                        {
                            ChooseDefaultOwner();
                        }
                    }
                }
            }
        }

        if(defaultChoice != null){
            if(defaultChoice.speaker != null){
                transform.position = defaultChoice.speaker.transform.position;
            }
        }

        if(Input.GetKeyDown(KeyCode.Alpha1)){
            Choose1();
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            Choose2();
        }
        else if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            Choose3();
        }
        else if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            Choose4();
        }
    }

    public void ShowChoices(){
        if (controller != null)
        {
            Conversation_Line current = controller.GetCurrentLine();
            if (current != null)
            {
                if (current.chooseTime > 0)
                {
                    countdownTimer.maxValue = current.chooseTime;
                    countdownTimer.value = current.chooseTime;
                    Debug.Log("setting choose timer: " + current.chooseTime);
                } else {
                    countdownTimer.value = 0;
                    countdownTimer.maxValue = 1;
                }
                allowChoice = true;
                choice1 = current.GetChoice(1);
                choice2 = current.GetChoice(2);
                choice3 = current.GetChoice(3);
                choice4 = current.GetChoice(4);
                defaultChoice = controller.GetLine(current.GetDefaultChoice());

                if(choice1 != null){
                    choice1Text.text = choice1.GetDisplayText(controller.language);
                } else {
                    choice1Text.text = "";
                }
                if (choice2 != null)
                {
                    choice2Text.text = choice2.GetDisplayText(controller.language);
                } else {
                    choice2Text.text = "";
                }
                if (choice3 != null)
                {
                    choice3Text.text = choice3.GetDisplayText(controller.language);
                } else {
                    choice3Text.text = "";
                }
                if (choice4 != null)
                {
                    choice4Text.text = choice4.GetDisplayText(controller.language);
                } else {
                    choice4Text.text = "";
                }
            }
            allowChoice = true;
        }
        if (animator != null)
        {
            animator.SetBool("visible", false);
            animator.SetBool("visible", true);
        }
        if(showSound != null){
            showSound.Play();
        }

    }

    public void HideChoices(){
        allowChoice = false;
        if(animator != null){
            animator.SetBool("visible", false);
        }
    }

    public void Choose1(){
        if(allowChoice && choice1Text != null && choice1Text.text.Length > 0){
            HideChoices();
            SendCustomNetworkEvent(NetworkEventTarget.Owner, "Choose1Owner");
        }
    }

    public void Choose1Owner(){
        if (controller != null)
        {
            if(choice1 != null){
                controller.PlayLineOwner(choice1.GetId());
            }
        }
    }

    public void Choose2()
    {
        if (allowChoice && choice2Text != null && choice2Text.text.Length > 0)
        {
            HideChoices();
            SendCustomNetworkEvent(NetworkEventTarget.Owner, "Choose2Owner");
        }
    }

    public void Choose2Owner(){
        if (controller != null)
        {
            if(choice2 != null){
                controller.PlayLineOwner(choice2.GetId());
            }
        }
    }


    public void Choose3()
    {
        if (allowChoice && choice3Text != null && choice3Text.text.Length > 0)
        {
            HideChoices();
            SendCustomNetworkEvent(NetworkEventTarget.Owner, "Choose3Owner");
        }
    }

    public void Choose3Owner(){
        if (controller != null)
        {
            if(choice3 != null){
                controller.PlayLineOwner(choice3.GetId());
            }
        }
    }


    public void Choose4()
    {
        if (allowChoice && choice4Text != null && choice4Text.text.Length > 0)
        {
            HideChoices();
            SendCustomNetworkEvent(NetworkEventTarget.Owner, "Choose4Owner");
        }
    }

    public void Choose4Owner(){
        if (controller != null)
        {
            if(choice4 != null){
                controller.PlayLineOwner(choice4.GetId());
            }
        }
    }

    public void ChooseDefaultOwner()
    {
        if (controller != null && defaultChoice != null)
        {
            controller.PlayLineOwner(defaultChoice.GetId());
        } else {
            controller.PlayLineOwner(-1);
        }
    }
}
