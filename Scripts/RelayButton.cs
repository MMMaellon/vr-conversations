
using UdonSharp;
using UnityEngine;
using VRC.SDKBase;
using VRC.Udon;

public class RelayButton : UdonSharpBehaviour
{
    public UdonBehaviour target;
    public string customEvent;
    void Start()
    {
        
    }

    public override void Interact()
    {
        if(target != null){
            target.SendCustomEvent(customEvent);
        }
    }
}
