
using UdonSharp;
using UnityEngine;
using VRC.SDKBase;
using VRC.Udon;
using VRC.Udon.Common.Interfaces;

public class Speaker : UdonSharpBehaviour
{
    private int id;
    public string displayName = "Unknown Voice";
    public AudioSource talkSound;
    private Conversation conversation_controller;

    [Header("Leave lines empty to auto fill based off children")]
    public Conversation_Line[] lines;
    void Start()
    {
        for (int i = 0; i < lines.Length; i++)
        {
            if(lines[i] != null){
                lines[i].RegisterSpeaker(this);
            }
        }
    }

    public void AutoFill(){
        lines = GetComponentsInChildren<Conversation_Line>();
        for (int i = 0; i < lines.Length; i++){
            if(lines[i] != null){
                lines[i].RegisterSpeaker(this);
            }
        }
    }

    public void SetId(int new_id, Conversation controller){
        id = new_id;
        conversation_controller = controller;
    }

    public void TakeOver()
    {
        SendCustomNetworkEvent(NetworkEventTarget.Owner, "TakeOverOwner");
    }
    public void TakeOverOwner()
    {
        Debug.Log("Setting Speaker Owner: " + id);
        if (conversation_controller != null)
        {
            conversation_controller.SetSpeakerOwner(id);
        }
    }

    public int GetId(){
        return id;
    }
}
