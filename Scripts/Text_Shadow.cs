
using UdonSharp;
using UnityEngine;
using VRC.SDKBase;
using VRC.Udon;
using UnityEngine.UI;

public class Text_Shadow : UdonSharpBehaviour
{

    public Text primaryText = null;
    public Text shadowText = null;
    void Start()
    {
        
    }

    private void LateUpdate(){
        if(shadowText != null && primaryText != null){
            shadowText.text = primaryText.text;
            shadowText.resizeTextForBestFit = primaryText.resizeTextForBestFit;
            shadowText.font = primaryText.font;
            shadowText.fontSize = primaryText.fontSize;
        }
    }
}
