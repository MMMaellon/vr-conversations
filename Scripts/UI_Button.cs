
using UdonSharp;
using UnityEngine;
using VRC.SDKBase;
using VRC.Udon;

public class UI_Button : UdonSharpBehaviour
{

    public UdonSharpBehaviour udon;
    public string functionName = "Interact";
    void Start()
    {
        
    }

    override public void Interact()
    {
        if(udon != null && udon != this){
            udon.SendCustomEvent(functionName);
        }
    }
}
