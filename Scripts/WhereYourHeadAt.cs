
using UdonSharp;
using UnityEngine;
using VRC.SDKBase;
using VRC.Udon;

public class WhereYourHeadAt : UdonSharpBehaviour
{
    public float smoothing = 0.9f;
    void Start()
    {
        
    }

    void Update(){
        transform.position = (Networking.LocalPlayer.GetTrackingData(VRCPlayerApi.TrackingDataType.Head).position * Mathf.Max(0, 1 - smoothing) + transform.position * Mathf.Min(smoothing, 1.0f));
    }
}
